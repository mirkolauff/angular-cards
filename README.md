# AngularCards

A simple app I used to get to know the creation of Angular components.

## [Check out live demo.](https://angular-cards-nu.now.sh/)

![Example](src/assets/example.gif)

## Local Development

### Requirements

- node >= 12.16

### Installation

    $ npm i

### Running app

    $ npm start

## Made with

[![Angular](src/assets/docs/angular-icon.png)](https://angular.io) [![Bulma](src/assets/docs/bulma-icon.jpg)](https://bulma.io/) [![Now.sh](src/assets/docs/now-sh-icon.png)](https://now.sh/)
